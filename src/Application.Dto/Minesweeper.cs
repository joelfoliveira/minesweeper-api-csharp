﻿namespace Minesweeper.Application.Dto
{
    using System;
    using System.Collections.Generic;

    public class Minesweeper
    {
        public Guid Id { get; set; }

        public int Score { get; set; }

        public IEnumerable<Cell> Cells { get; set; }
    }
}