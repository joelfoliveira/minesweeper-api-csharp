﻿namespace Minesweeper.Application.Services
{
    using System.Threading.Tasks;
    using Minesweeper.Application.Services.Mappers;
    using Dto = Minesweeper.Application.Dto;
    using Model = Minesweeper.Domain.Model;

    public class MinesweeperService : IMinesweeperService
    {
        public async Task<Dto.Minesweeper> CreateNewGame()
        {
            var minesweeper = new Model.Minesweeper();
            return minesweeper.ToDto();
        }
    }
}