﻿namespace Minesweeper.Presentation.WebApi.Controllers.Version1
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Minesweeper.Application.Services;

    [ApiController]
    [Route("v1/minesweepers")]
    public class MinesweeperController : ControllerBase
    {
        private readonly IMinesweeperService minesweeperService;

        public MinesweeperController(IMinesweeperService minesweeperService)
        {
            this.minesweeperService = minesweeperService;
        }

        [HttpPost, Route("")]
        public async Task<IActionResult> Create()
        {
            var minesweeper = await this.minesweeperService.CreateNewGame();

            return this.Created($"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}/v1/minesweepers/{minesweeper.Id}", minesweeper);
        }
    }
}