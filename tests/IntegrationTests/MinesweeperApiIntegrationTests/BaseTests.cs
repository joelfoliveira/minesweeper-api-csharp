﻿namespace MinesweeperApiIntegrationTests
{
    using System;
    using System.IO;
    using System.Net.Http;
    using Microsoft.Extensions.Configuration;

    public class BaseTests
    {
        protected readonly string BaseAddress;
        protected readonly HttpClient Client;

        private readonly IConfigurationRoot configuration;

        public BaseTests()
        {
            this.configuration = this.ReadConfiguration();

            this.BaseAddress = this.configuration["MinesweeperApiHost"];

            this.Client = new HttpClient
            {
                BaseAddress = new Uri(this.BaseAddress)
            };
        }

        private IConfigurationRoot ReadConfiguration()
        {
            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

#if !DEBUG
            configBuilder.AddJsonFile("appsettings.release.json", optional: true);
#endif

            return configBuilder.Build();
        }
    }
}