namespace MinesweeperApiIntegrationTests
{
    using System.Net;
    using System.Threading.Tasks;
    using Newtonsoft.Json.Linq;
    using Xunit;

    public class MinesweeperTests : BaseTests
    {
        [Fact]
        public async Task MinesweeperApi_CreateNewGame_201IsReturnedWithGameLocations()
        {
            // Arrange
            var expectedStatusCode = HttpStatusCode.Created;
            var endpoint = "/v1/minesweepers/";

            // Act
            var result = await this.Client.PostAsync(endpoint, null);
            dynamic game = JObject.Parse(await result.Content.ReadAsStringAsync());

            // Assert
            Assert.Equal(expectedStatusCode, result.StatusCode);
            Assert.Equal(this.BaseAddress + endpoint + game.id, result.Headers.Location.OriginalString);
        }
    }
}